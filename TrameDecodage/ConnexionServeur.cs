﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace IABatailleDuCafe.TrameDecodage
{
    public class ConnexionServeur
    {
        //Attributs
        private Socket socketIA;
        private Encoding ascii = Encoding.ASCII;
        private string derniereReponse = "";
        private int positionX, positionY;

        //Constructeurs
        public ConnexionServeur(string ip, int port)
        {
            //Connection Serveur
            try
            {
                socketIA = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socketIA.Connect(ip, port);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(-1);
            };
        }

        //Methodes

        /* Retourne la trame envoyé par le serveur correspondant à la map sous la forme
         * d'une chaîne de caractères.
         * param : Ø
         * return : la trame : string
         */
        public string GetTrame()
        {

            string trameMap = "";
            try
            {
                //recuperation trame en octets
                byte[] buffer = new byte[1024];
                socketIA.Receive(buffer);
                //transformation en chaîne de cractères
                trameMap = ascii.GetString(buffer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Echec recuperation de la map");
                Console.ReadKey();
                Environment.Exit(-1);
            };

            return trameMap;
        }

        /* Envoie au serveur le coup du client
         * d'une chaîne de caractères.
         * param : coordonnées du coup
         * return : Ø
         */
        public void EnvoieCoup(int ligne, int colonne)
        {
            string data = String.Format("A:{0}{1}", ligne, colonne);
            Console.WriteLine("Coup Client : " + data);
            socketIA.Send(ascii.GetBytes(data));
        }

        /* Recupere la reponse du serveur
         * param :  Ø
         * return : string : la reponse du serveur 4 caractères
         */
        public string GetReponse()
        {
            try
            {
                byte[] buffer = new byte[1024];
                int byteTaille = socketIA.Receive(buffer, sizeof(Char) * 4, 0);
                derniereReponse = ascii.GetString(buffer, 0, byteTaille);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Erreur communication serveur");
                Console.ReadKey();
                Environment.Exit(-1);
            };
            return derniereReponse;
        }

        /* Reçoit le coup du serveur
         * param :  Ø
         * return : un booléen qui permet de savoir si le serveur a bien envoyer un coup
         */
        public bool RecevoirCoupServeur()
        {
            string reponse = GetReponse();
            Console.WriteLine(reponse);
            if (reponse == "FINI")
                return false;
            positionX = Convert.ToInt32(reponse.Substring(2,1));
            positionY = Convert.ToInt32(reponse.Substring(3,1));
            return true;
        }

        //Getter

        public string GetDerniereReponse()
        {
            return derniereReponse;
        }

        public int GetPositionX()
        {
            return positionX;
        }

        public int GetPositionY()
        {
            return positionY;
        }
             
    }
}
