﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IABatailleDuCafe.TrameDecodage
{
    public class MapDecodeur
    {
        //Attributs
        private int[,] representationNumerique;
        private char[,] map;
        private int indiceLettreParcelle;

        //Constructeurs
        public MapDecodeur()
        {
            representationNumerique = new int[10, 10];
            map = new char[10, 10];
            //initialisation valeurs
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    map[ligne, colonne] = '0';
                    representationNumerique[ligne, colonne] = 0;
                }
            }
            indiceLettreParcelle = 0;
        }

        //Méthodes

        /* Decoupage de la trame passé en paramètre pour recuperer les representation numeriques
         *  et les placer dans un tableau 2D d'entiers
         *  param : trame : string
         *  return : Ø(change les valeurs du talbleau en attribut)
         */
        private void DecoupageTrame(string trame)
        {
            string[] lignes = new string[10];
            string[] cases_as_str = new string[10];
            //Decoupe la trame pour récupérer les representationNumeriquerésentations des unités par lignes
            lignes = trame.Split('|');
            for (int ligne = 0; ligne < 10; ligne++)
            {
                //Decoupe les lignes pour récupérer les representationNumeriquerésentations numériques des unités de la ligne
                cases_as_str = lignes[ligne].Split(':');
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    representationNumerique[ligne, colonne] = Int32.Parse(cases_as_str[colonne]);
                }
            }
        }

        //Check si la representation numèrique d'une unité correspond à la mer
        private bool IsSea(int representationNumerique)
        {
            return (representationNumerique >= 64);
        }

        //Check si la representation numèrique d'une unité correspond à la forêt
        private bool IsForest(int representationNumerique)
        {
            return (representationNumerique < 64 && representationNumerique > 32);
        }


        /* Renvoie un tableau de booléen correspond aux frontière de l'unité à partir de sa representation numérique
         * Le tableau de bool correspond dans l'ordre aux frontières Nord Ouest Sud Est
         *  param : representationNumerique : int
         *  return : frontiere : bool[4]
         */
        private Boolean[] CheckBorder(int representationNumerique)
        {
            bool[] tmp_frontiere = { false, false, false, false };

            for (int puissance = 3; puissance > -1; puissance--)
            {
                if (representationNumerique / (int)Math.Pow(2, puissance) == 1)
                {
                    tmp_frontiere[puissance] = true;
                    representationNumerique = representationNumerique % (int)Math.Pow(2, puissance);
                }

            }

            return tmp_frontiere;
        }

        /* Fonction recursive qui donne aux unités des parcelles leurs lettres correspondent et appelle la fonction avec
         * les unites voisines de la même parcelle
         * Condition d'arrêt : Toutes les parcelles adjacentes sont traités ou sont dans le direction d'une frontière
         *  param : ligne et colonne : int et  int : position de l'unité sur la carte
         *          indiceLettreParcelle : int : indice correspondant à la lettre de la parcelle actuelle(=position dans l'alphabet)
         *  return : Ø(change les lettres dans l'attributs map)
         */
        private void TraitementParcelle(int ligne, int colonne, char lettreParcelle)
        {


            if(map[ligne, colonne] == '0')
                map[ligne, colonne] = lettreParcelle;

            Boolean[] frontiere = CheckBorder(representationNumerique[ligne, colonne]);

            //Nord
            if ( ligne > 0 && !(frontiere[0] || map[ligne-1, colonne] != '0'))
                TraitementParcelle(ligne - 1, colonne, lettreParcelle);
            //Ouest
            if (colonne > 0 && !(frontiere[1] || map[ligne, colonne - 1] != '0'))
                TraitementParcelle(ligne, colonne - 1, lettreParcelle);
            //Sud
            if (ligne < 9 && !(frontiere[2] || map[ligne + 1, colonne] != '0'))
                TraitementParcelle(ligne + 1, colonne, lettreParcelle);
            //Est
            if (colonne < 9 && !(frontiere[3] || map[ligne, colonne + 1] != '0'))
                TraitementParcelle(ligne, colonne + 1, lettreParcelle);
        }

        /* Fonction qui traite chaque unité pour lui donner sa lettre si elle correspond à la Mer ou la Foret
         * sinon sa parcelle est traitée
         *  param : ligne et colonne : int et  int : position de l'unité sur la carte
         *  return : Ø(change les lettres dans l'attributs map)
         */
        private void TraitementUnite(int ligne, int colonne) {
            char[] alpha = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            if (IsSea(representationNumerique[ligne, colonne]))
            {
                map[ligne, colonne] = 'M';
            }
            else
            {
                if (IsForest(representationNumerique[ligne, colonne]))
                {
                    map[ligne, colonne] = 'F';
                }
                else {
                    TraitementParcelle(ligne, colonne, alpha[indiceLettreParcelle]);
                    indiceLettreParcelle++;
                }
            }

        }

        /* Prend la trame en paramètre, la découpe, traite chaque unité de la carte avec sa reprèsentation numèrique
         * et renvoie la map décodée
         *  param : trame : string
         *  return : map : char[,] : l'attribut map qui a été modifié par d'autres fonctions de la classe(map décodée)
         */
        public char[,] DecodageTrame(string trame)
        {
            DecoupageTrame(trame);

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if(map[ligne, colonne] == '0')
                        TraitementUnite(ligne, colonne);
                }
            }

            return map;
            
        }
    }
}
