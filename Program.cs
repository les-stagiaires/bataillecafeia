﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using IABatailleDuCafe.TrameDecodage;
using IABatailleDuCafe.Misc;
using IABatailleDuCafe.IA;

namespace IABatailleDuCafe
{
    class Program
    {

        static void Main(string[] args)
        {
            //infos serveur
            string adresseIP = "127.0.0.1";
            int portApp = 1213;

            //Connection au serveur 
            ConnexionServeur connexionIA = new ConnexionServeur(adresseIP, portApp);

            //Recuperation de la map
            
            //Decodage
            MapDecodeur decodeur = new MapDecodeur();
            Partie v_partie = new Partie(new Map(decodeur.DecodageTrame(connexionIA.GetTrame())));


            IntelligenceArtificielle claptrap = new IntelligenceArtificielle();

            //Affichage map
            v_partie.Affichage();

            //Jeu
            //Boucle de jeu         
            //Client joue blanc et serveur joue noir
            do
            {
                //tour client
                claptrap.RechercheProchainCoup(v_partie);
                connexionIA.EnvoieCoup(claptrap.getCoupX(), claptrap.getCoupY());
                if(connexionIA.GetReponse() == "VALI")
                    v_partie.JouerCoup(claptrap.getCoupX(), claptrap.getCoupY(), Case.Blanche);
                //tour serveur
                if(connexionIA.RecevoirCoupServeur())
                    v_partie.JouerCoup(connexionIA.GetPositionX(), connexionIA.GetPositionY(), Case.Noire);
                v_partie.TourSuivant();

            } while (connexionIA.GetReponse() == "ENCO");

            //affichage score serveur
            Console.WriteLine(connexionIA.GetReponse());

            //Affichage score client
            v_partie.CalculScore();
            Console.WriteLine(String.Format("Calcul Score Client : Score client : {0} Score serveur : {1}", v_partie.GetScoreBlanc(), v_partie.GetScoreNoir()));

            //Attente
            Console.ReadKey();
        }
    }
}
