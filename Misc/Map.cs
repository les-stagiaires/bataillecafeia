﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace IABatailleDuCafe.Misc
{
    public enum Case
    {
        Vide,
        Blanche,
        Noire
    }

    public class Map
    {
        //Attributs
        private char[,] mapParcelle;
        private Case[,] placementGraine;
        
        //Constructeurs
        public Map(char[,] parcelle)
        {
            this.mapParcelle = parcelle;
            this.placementGraine = new Case[10,10];
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++) {
                    this.placementGraine[ligne, colonne] = Case.Vide;
                }
            }
        }

        public Map(Map map)
        {
            this.mapParcelle = new Char[10, 10];
            this.placementGraine = new Case[10, 10];
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    mapParcelle[ligne, colonne] = map.getParcelleCase(ligne, colonne);
                    placementGraine[ligne, colonne] = map.getGraineCase(ligne, colonne);
                }
            }
        }

        //Afifche la map 
        public void MapAffichage()
        {
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    //change la couleur des carcatères en fonction de leur parcelle
                    if (mapParcelle[ligne, colonne] == 'M')
                        Console.ForegroundColor = ConsoleColor.DarkBlue;
                    else
                    {
                        if (mapParcelle[ligne, colonne] == 'F')
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                        else
                            Console.ForegroundColor = (ConsoleColor)(mapParcelle[ligne, colonne] % 15 + 1);
                    }
                    Console.Write("{0} ", mapParcelle[ligne, colonne]);
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        //Getter et Setter 

        public void setCase(int x, int y, Case couleur)
        {
            placementGraine[x, y] = couleur;
        }

        public Case getGraineCase(int x, int y)
        {
            return placementGraine[x, y];
        }

        public char getParcelleCase(int x, int y)
        {
            return mapParcelle[x, y];
        }

    }
}
