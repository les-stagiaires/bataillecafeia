﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IABatailleDuCafe.Misc;

namespace IABatailleDuCafe.IA
{
    public class IntelligenceArtificielle
    {

        //Attributs
        private int positionX, positionY;

        //Methodes
       

            /* Fonction qui prend un noeud de l'arbre des coup possibles et retourne une valeur sachant qu'on cherche à maximiser 
             * la difference lors de nos coup et l'adversaire à les miniser(demander à Thomas pour explications c'est chaud par commentaire d'expliquer)
            * param : un noeud correspond à un moment de la partie dans l'arbre de possibilités des coup, la profondeur pour limiter la taille de l'arbre, 
            * isClient pour savoir si c'est au tour du client ou du serveur 
            * return : la valeur correpondant(faut juste savoir que plus c'est grand mieux c'est elle correspond à scoreNoir - scoreBlanc)
            */
            private int minmax(Partie noeud, int profondeur, bool isClient)
        {
            noeud.CalculScore();
            int poids = noeud.GetScoreNoir() - noeud.GetScoreBlanc();
            //Fin fonction recursive
            if (profondeur == 0 || noeud.IsPartieFini())
                return poids;

            if (profondeur == 3)
                Console.WriteLine("Recherche...");

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (noeud.IsJouableIci(ligne, colonne))
                    {
                        Partie fils = new Partie(noeud);
                        if (isClient) {
                            poids = int.MinValue;
                            fils.JouerCoup(ligne, colonne, Case.Blanche);
                            poids = Math.Max(poids, minmax(fils, profondeur - 1, false));
                        }
                        else
                        {
                            poids = int.MaxValue;
                            fils.JouerCoup(ligne, colonne, Case.Noire);
                            poids = Math.Min(poids, minmax(fils, profondeur - 1, true));
                        }
                    }
                }
            }

            return poids;
        }

        /* Fonction qui cherche le meilleur coup a joué, elle prend tout les coup possibles et les envoie dans une fonction récursive 
         * qui va parcourir l'arbre des coup suivants possible et renvoyer un poids en considérant qu'ont veut maximiser la différence 
         * de score et l'adversaire la diminuer(demander à Thomas pour plus d'explications)
         * param : la partie au moment t
         * return : mise à jour des attributs qui correspondent au coordonnées du meilleur coup
         */
        public void RechercheProchainCoup(Partie partie)
        {
            int x=0, y=0, profondeur;
            int poids = int.MinValue, tempsPoids = 0;

            Console.WriteLine("Debut Recherche");

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if(partie.IsJouableIci(ligne, colonne))
                    {
                        Partie temp = new Partie(partie);
                        temp.JouerCoup(ligne, colonne, Case.Blanche);

                        //Profondeur jouée au tour 1 et après, si modifié change le temps que met l'algo pour une réponse
                        if (partie.GetTour() == 0)
                            profondeur = 2;
                        else
                            profondeur = 3;

                        tempsPoids = minmax(temp, profondeur, false);
                        if ( tempsPoids > poids)
                        {
                            poids = tempsPoids;
                            x = ligne; y = colonne;
                        }
                    }
                }
            }
            Console.WriteLine("Fin Recherche");


            positionX = x;
            positionY = y;
        }

        //Getter

        public int getCoupX()
        {
            return positionX;
        }

        public int getCoupY()
        {
            return positionY;
        }

    }
}
