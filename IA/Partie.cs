﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IABatailleDuCafe.Misc;

namespace IABatailleDuCafe.IA
{
    public class Partie
    {
        //Attributs
        private Map mapDeJeu;
        private int tour;
        private char lastParcelle;
        private int lastX, lastY;
        private int scoreBlanc, scoreNoir;


        //Constructeurs
        public Partie(Map mapDeJeu)
        {
            this.mapDeJeu = mapDeJeu;
            tour = 0;
            lastParcelle = '0';
            lastX = 0;
            lastY = 0;
            scoreBlanc = 0;
            scoreNoir = 0;
        }

        public Partie(Partie partie)
        {
            this.mapDeJeu = new Map(partie.GetMap());

            lastParcelle = partie.GetLastParcelle();
            lastX = partie.GetLastX();
            lastY = partie.GetLastY();
            scoreBlanc = partie.GetScoreBlanc();
            scoreNoir = partie.GetScoreNoir();        
        }

        //Méthodes

        /* Fonction d'affichage de la map de la partie
         *  param : Ø
         *  return : Affichage de la map de la partie sur la console
         */
        public void Affichage()
        {
            mapDeJeu.MapAffichage();
        }

        /* Fonction recursive qui parcourt une parcelle de par ses cellules adjacentes pour obtenir le nombre de graines 
         * de chaque couleur située dans la parcelle ainsi que la taille de ma parcelle, ces informations étant nécessaires 
         * au calcul du score 
         *  param : ligne et colonne correspond au coordonnée d'une cellule de la parcelle, nombre de graines passé en référence 
         *  pour l'incrémenter à chaque fois qu'une cellule posséde une graine, la taille est aussi passé en référence pour l'incrémenter 
         *  de 1 à chaque cellule traité, un tableau pour savoir quelle cellule ont déjà été traitées
         *  return : taille de la parcelle et le nombre de graines de chaque couleur
         */
        private void TraitementParcelle(int ligne, int colonne, ref int taille, ref int nombreGraineBlanche, ref int nombreGraineNoire, bool[,] tableauTraitement)
        {
            tableauTraitement[ligne, colonne] = true;
            taille++;
            if (mapDeJeu.getGraineCase(ligne, colonne) == Case.Noire)
                nombreGraineNoire++;

            if (mapDeJeu.getGraineCase(ligne, colonne) == Case.Blanche)
                nombreGraineBlanche++;

            //Nord
            if (ligne > 0 && mapDeJeu.getParcelleCase(ligne, colonne) == mapDeJeu.getParcelleCase(ligne-1, colonne) && tableauTraitement[ligne-1, colonne] == false)
                TraitementParcelle(ligne-1, colonne, ref taille, ref nombreGraineBlanche, ref nombreGraineNoire, tableauTraitement);
            //Ouest
            if (colonne > 0 && mapDeJeu.getParcelleCase(ligne, colonne) == mapDeJeu.getParcelleCase(ligne, colonne-1) && tableauTraitement[ligne, colonne-1] == false)
                TraitementParcelle(ligne, colonne-1, ref taille, ref nombreGraineBlanche, ref nombreGraineNoire, tableauTraitement);
            //Sud
            if(ligne < 9 && mapDeJeu.getParcelleCase(ligne, colonne) == mapDeJeu.getParcelleCase(ligne+1, colonne) && tableauTraitement[ligne+1, colonne] == false)
                TraitementParcelle(ligne+1, colonne, ref taille, ref nombreGraineBlanche, ref nombreGraineNoire, tableauTraitement);
            //Est
            if (colonne < 9 && mapDeJeu.getParcelleCase(ligne, colonne) == mapDeJeu.getParcelleCase(ligne, colonne+1) && tableauTraitement[ligne, colonne+1] == false)
                TraitementParcelle(ligne, colonne+1, ref taille, ref nombreGraineBlanche, ref nombreGraineNoire, tableauTraitement);
        }

        /* Fonction qui est appellée pour haque parcelle non traitée et qui appel une fonction recursive pour récupérer sa taille et le 
         * nombre de graines de chaque couleur situées à l'intérieur et l'ajoute au score de la couleur correspondante
         *  param : ligne et colonne correspond au coordonnée d'une cellule de la parcelle,
         *  un tableau pour savoir quelle cellule ont déjà été traitées et le score de chaque couleur pour pouvoir l'incrémenter
         *  return : une mise à jour des scores avec les points de la parcelle correspondante au coordonnées
         */
        private void GetScoreParcelle(int ligne, int colonne, bool[,] tableauTraitement, ref int scoreBlanc, ref int scoreNoir)
        {
            int nombreGraineBlanche = 0, nombreGraineNoire = 0;
            int taille = 0;
            TraitementParcelle(ligne, colonne, ref taille, ref nombreGraineBlanche, ref nombreGraineNoire, tableauTraitement);
            if (nombreGraineBlanche != nombreGraineNoire)
            {
                if (nombreGraineNoire > nombreGraineBlanche)
                    scoreNoir += taille;
                if (nombreGraineBlanche > nombreGraineNoire)
                    scoreBlanc += taille;
            }
        }

        /* Fonction qui est appellée pour haque parcelle non traitée et qui appel une fonction recursive pour récupérer sa taille et le 
         * nombre de graines de chaque couleur situées à l'intérieur et l'ajoute au score de la couleur correspondante
         *  param : ligne et colonne correspond au coordonnée d'une cellule de la parcelle,
         *  un tableau pour savoir quelle cellule ont déjà été traitées et le score de chaque couleur pour pouvoir l'incrémenter
         *  return : une mise à jour des scores avec les points de la parcelle correspondante au coordonnées
         */
        private void TraitementBonus(int ligne, int colonne, ref int taille, Case couleur, bool[,] tableauTraitement)
        {

            tableauTraitement[ligne, colonne] = true;
            taille++;

            //Console.WriteLine(string.Format("{0}{1}-{2}", ligne, colonne, couleur));

            //Nord
            if (ligne > 0 && couleur == mapDeJeu.getGraineCase(ligne - 1, colonne) && tableauTraitement[ligne - 1, colonne] == false)
                TraitementBonus(ligne - 1, colonne, ref taille, couleur, tableauTraitement);
            //Ouest
            if (colonne > 0 && couleur == mapDeJeu.getGraineCase(ligne, colonne - 1) && tableauTraitement[ligne, colonne - 1] == false)
                TraitementBonus(ligne, colonne - 1, ref taille, couleur, tableauTraitement);
            //Sud
            if (ligne < 9 && couleur == mapDeJeu.getGraineCase(ligne + 1, colonne) && tableauTraitement[ligne + 1, colonne] == false)
                TraitementBonus(ligne + 1, colonne, ref taille, couleur, tableauTraitement);
            //Est
            if (colonne < 9 && couleur == mapDeJeu.getGraineCase(ligne, colonne + 1) && tableauTraitement[ligne, colonne + 1] == false)
                TraitementBonus(ligne, colonne + 1, ref taille, couleur, tableauTraitement);
        }


        /* Fonction qui calcule le score bonus d'une couleur, elle parcourt la map et pour chaque graine de cette couleur 
         * non traitée elle appelle une fonction récursive qui parcourt la parcelle de graine pour déterminer sa taille
         * et renvoie la taille de la plus grande parcelle
         *  param : couleur de graine dont on veut connaître le score bonus
         *  return : le score bonus pour cette couleur
         */
        private int GetBonusScore(Case couleur)
        {

            bool[,] parcelleTraite = new bool[10, 10];
            int taille = 0, best = 0;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    parcelleTraite[ligne, colonne] = false;
                }
            }


            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    taille = 0;
                    if (mapDeJeu.getParcelleCase(ligne, colonne) != 'M' && mapDeJeu.getParcelleCase(ligne, colonne) != 'F' && parcelleTraite[ligne, colonne] == false && couleur == mapDeJeu.getGraineCase(ligne, colonne))
                        TraitementBonus(ligne, colonne, ref taille, couleur, parcelleTraite);
                        
                    if (taille > best)
                        best = taille;
                }
            }

            return best;
        }

        /* Fonction qui calcule le score et le met à jour dans les attributs de la partie en appelant les fonctions 
         * qui calcule le score de controle des parcelle et le score bonus
         *  param : Ø
         *  return : mise à jour du score
         */
        public void CalculScore()
        {
            bool[,] parcelleTraite = new bool[10, 10];
            int scoreBlanc = 0, scoreNoir = 0;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    parcelleTraite[ligne, colonne] = false;
                }
            }

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (mapDeJeu.getParcelleCase(ligne, colonne) != 'M' && mapDeJeu.getParcelleCase(ligne, colonne) != 'F' && parcelleTraite[ligne, colonne] == false)
                         GetScoreParcelle(ligne, colonne, parcelleTraite, ref scoreBlanc, ref scoreNoir);
                }
            }


            this.scoreBlanc = scoreBlanc + GetBonusScore(Case.Blanche);
            this.scoreNoir = scoreNoir + GetBonusScore(Case.Noire);

        }

        /* Fonction vérifie si on peut jouer une graine à certaine coordonnées
         *  param : coordonnés de la cellule dont on veut vérifié si on peut jouer ou pas
         *  return : résultat du test
         */
        public bool IsJouableIci(int ligne, int colonne)
        {
            //on peut pas jouer sur les case Mer et foret
            if(mapDeJeu.getParcelleCase(ligne,colonne) == 'M' || mapDeJeu.getParcelleCase(ligne, colonne) == 'F')
                return false;
            else
            {
                //On ne peut jouer que dans une case vide
                if(mapDeJeu.getGraineCase(ligne,colonne) != Case.Vide)
                    return false;
                else
                {
                    //Au tour 1 pose libre
                    if (tour == 0)
                        return true;
                    else
                    {
                        //pas la même parcelle mais meme ligne et colonne que la derniere graine
                        if (mapDeJeu.getParcelleCase(ligne, colonne) == lastParcelle || (lastX != ligne && lastY != colonne))
                            return false;
                        else
                            return true;
                    }
                }
            }

            
        }

        /* Fonction pour jouer un coup
         *  param : coordonnés de la cellule et couleur de la graine
         *  return : Ø
         */
        public void JouerCoup(int ligne, int colonne, Case couleur)
        {
            mapDeJeu.setCase(ligne, colonne, couleur);
            lastParcelle = mapDeJeu.getParcelleCase(ligne, colonne);
            lastX = ligne;
            lastY = colonne;
        }

        /* Fonction pour incrémenter la valeur du tour
         *  param : Ø
         *  return : Ø
         */
        public void TourSuivant()
        {
            tour++;
        }


        /* Fonction pour vérifier si la partie est finie, check les graines restantes et si on peut encore jouer
         *  param : Ø
         *  return : resultat du test
         */
        public bool IsPartieFini()
        {
            if (tour == 28)
                return true;
            else
            {
                bool fin = true;
                int ligne = 0, colonne = 0;
                while (ligne < 10 && fin)
                {
                    colonne = 0;
                    while (colonne < 10 && fin)
                    {
                        if (IsJouableIci(ligne, colonne))
                            fin = false;
                        colonne++;
                    }
                    ligne++;
                }
                return fin;
            }
        }

            //Getter
        public int GetScoreBlanc()
        {
            return scoreBlanc;
        }

        public int GetScoreNoir()
        {
            return scoreNoir;
        }

        public int GetLastX()
        {
            return lastX;
        }

        public int GetLastY()
        {
            return lastY;
        }

        public char GetLastParcelle()
        {
            return lastParcelle;
        }

        public int GetTour()
        {
            return tour;
        }

        public Map GetMap()
        {
            return mapDeJeu;
        }

        

    }
}
